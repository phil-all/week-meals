<?php

namespace App\Tests\Behat;

use Exception;
use Behat\Mink\Mink;
use App\Service\String\UrlHandler;
use Behat\Mink\Element\DocumentElement;
use Behat\MinkExtension\Context\MinkContext;
use Symfony\Component\Routing\RouterInterface;

use function PHPUnit\Framework\assertEquals;

/**
 * This context class contains the definitions of the steps used by the security
 * feature files:
 * - authentification
 * - access
 *
 * @author Philippe Allard-Latour <phil.all.dev@gmail.com>
 */
class WebContext extends MinkContext
{
    private const LOGIN_ROUTE = 'app_login';
    private const USER_FIELD  = 'username';
    private const PASS_FIELD  = 'password';
    private const LOGIN_BTN   = 'submit_login_form_btn';

    public function __construct(
        private Mink $mink,
        private UrlHandler $urlHandler,
        private RouterInterface $router
    ) {
        // empty body
    }

    // User Part
    // *********

    /**
     * @Given I am anonymous user
     */
    public function iAmAnonymousUser(): void
    {
        $this->mink->resetSessions();
    }

    /**
     * @Given I am logged as user :userIdentifier with password :password
     */
    public function iAmLoggedAsUser(string $userIdentifier, string $password): void
    {
        $this->mink->resetSessions();
        $this->mink->getSession()->visit($this->router->generate(self::LOGIN_ROUTE));

        /** @var DocumentElement $loginPage */
        $loginPage = $this->mink->getSession()->getPage();

        $loginPage->fillField(self::USER_FIELD, $userIdentifier);
        $loginPage->fillField(self::PASS_FIELD, $password);

        $submit = $loginPage->findButton(self::LOGIN_BTN);

        if (empty($submit)) {
            throw new Exception(sprintf("No submit button at %s", $this->mink->getSession()->getCurrentUrl()));
        }

        $submit->click();
    }

    // Navigation Part
    // ***************

    /**
     * @Then I am redirected to :path
     */
    public function iAmRedirectedTo(string $path): void
    {
        $expectedUrl = $this->urlHandler->pathToAbsoluteUrl($path);

        $currentUrl = $this->urlHandler->formatLocalUrl($this->mink->getSession()->getCurrentUrl());

        assertEquals($expectedUrl, $currentUrl);
    }

    // Page content Part
    // *****************

    /**
     * @Then I should see error message :message
     */
    public function iShouldSeeMessage(string $message): void
    {
        $this->assertSession()->elementTextContains('xpath', '//div[@class="alert alert-danger"]', $message);
    }
}

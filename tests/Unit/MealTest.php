<?php

namespace App\Tests\Unit;

use App\Entity\Meal;
use App\Entity\User;
use App\Entity\Recipe;
use DateTimeImmutable;
use App\Entity\MealType;
use PHPUnit\Framework\TestCase;

class MealTest extends TestCase
{
    private Meal $meal;

    protected function setUp(): void
    {
        parent::setUp();
        $this->meal = new Meal();
    }

    public function testGetPlannedDate(): void
    {
        $value    = new DateTimeImmutable('now');
        $response = $this->meal->setDate($value);

        $this->assertInstanceOf(Meal::class, $response);
        $this->assertEquals($value, $this->meal->getDate());
    }

    public function testGetOwner(): void
    {
        $value    = new User();
        $response = $this->meal->setOwner($value);

        $this->assertInstanceOf(Meal::class, $response);
        $this->assertEquals($value, $this->meal->getOwner());
    }



    public function testGetRecipe(): void
    {
        $value    = new Recipe();
        $response = $this->meal->setRecipe($value);

        $this->assertInstanceOf(Meal::class, $response);
        $this->assertEquals($value, $this->meal->getRecipe());
    }

    public function testGetMealType(): void
    {
        $value    = new MealType();
        $response = $this->meal->setMealType($value);

        $this->assertInstanceOf(Meal::class, $response);
        $this->assertEquals($value, $this->meal->getMealType());
    }
}

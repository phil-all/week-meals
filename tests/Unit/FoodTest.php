<?php

namespace App\Tests\Unit;

use App\Entity\Food;
use App\Entity\Unit;
use PHPUnit\Framework\TestCase;
use App\Entity\RecipeIngredient;

class FoodTest extends TestCase
{
    private Food $food;

    protected function setUp(): void
    {
        parent::setUp();
        $this->food = new Food();
    }

    public function testGetName(): void
    {
        $value    = 'a food name';
        $response = $this->food->setName($value);

        $this->assertInstanceOf(Food::class, $response);
        $this->assertEquals($value, $this->food->getName());
    }

    public function testgetCaloriesPerHundredUnit(): void
    {
        $value    = 84;
        $response = $this->food->setCaloriesPerHundredUnit($value);

        $this->assertInstanceOf(Food::class, $response);
        $this->assertEquals($value, $this->food->getCaloriesPerHundredUnit());
    }

    public function testGetRecipeIngredients(): void
    {
        $ingredient_1 = new RecipeIngredient();
        $ingredient_2 = new RecipeIngredient();

        $this->assertEquals(true, $this->food->getRecipeIngredients()->isEmpty());

        $response = $this->food->addRecipeIngredient($ingredient_1);
        $this->food->addRecipeIngredient($ingredient_2);

        $this->assertInstanceOf(Food::class, $response);
        $this->assertEquals(true, $this->food->getRecipeIngredients()->contains($ingredient_1));
        $this->assertEquals(true, $this->food->getRecipeIngredients()->contains($ingredient_2));
        $this->assertEquals(2, $this->food->getRecipeIngredients()->count());

        $response = $this->food->removeRecipeIngredient($ingredient_2);

        $this->assertInstanceOf(Food::class, $response);
        $this->assertEquals(1, $this->food->getRecipeIngredients()->count());
    }

    public function testGetUnit(): void
    {
        $unit     = new Unit();
        $response = $this->food->setUnit($unit);

        $this->assertInstanceOf(Food::class, $response);
        $this->assertEquals($unit, $this->food->getUnit());
    }
}

<?php

namespace App\Tests\Unit;

use App\Entity\Meal;
use App\Entity\User;
use App\Entity\Recipe;
use App\Entity\RecipeTag;
use App\Entity\RecipeStep;
use PHPUnit\Framework\TestCase;
use App\Entity\RecipeIngredient;

class RecipeTest extends TestCase
{
    private Recipe $recipe;

    protected function setUp(): void
    {
        parent::setUp();
        $this->recipe = new Recipe();
    }

    public function testGetName(): void
    {
        $value    = 'a value';
        $response = $this->recipe->setName($value);

        $this->assertInstanceOf(Recipe::class, $response);
        $this->assertEquals($value, $this->recipe->getName());
    }

    public function testGetNumberOfServings(): void
    {
        $value    = 4;
        $response = $this->recipe->setNumberOfSevings($value);

        $this->assertInstanceOf(Recipe::class, $response);
        $this->assertEquals($value, $this->recipe->getNumberOfSevings());
    }

    public function testGetCaloriesPerServing(): void
    {
        $value    = 250;
        $response = $this->recipe->setCaloriesPerServing($value);

        $this->assertInstanceOf(Recipe::class, $response);
        $this->assertEquals($value, $this->recipe->getCaloriesPerServing());
    }

    public function testGetCookingTime(): void
    {
        $value    = 45;
        $response = $this->recipe->setCookingTime($value);

        $this->assertInstanceOf(Recipe::class, $response);
        $this->assertEquals($value, $this->recipe->getCookingTime());
    }

    public function testGetOwner(): void
    {
        $owner    = new User();
        $response = $this->recipe->setOwner($owner);

        $this->assertInstanceOf(Recipe::class, $response);
        $this->assertEquals($owner, $this->recipe->getOwner());
    }

    public function testGetMeals(): void
    {
        $meal_1 = new Meal();
        $meal_2 = new Meal();

        $this->assertEquals(true, $this->recipe->getMeals()->isEmpty());

        $response = $this->recipe->addMeal($meal_1);
        $this->recipe->addMeal($meal_2);

        $this->assertInstanceOf(Recipe::class, $response);
        $this->assertEquals(true, $this->recipe->getMeals()->contains($meal_1));
        $this->assertEquals(true, $this->recipe->getMeals()->contains($meal_2));
        $this->assertEquals(2, $this->recipe->getMeals()->count());

        $response = $this->recipe->removeMeal($meal_2);

        $this->assertInstanceOf(Recipe::class, $response);
        $this->assertEquals(1, $this->recipe->getMeals()->count());
    }

    public function testGetRecipeSteps(): void
    {
        $step_1 = new RecipeStep();
        $setp_2 = new RecipeStep();

        $this->assertEquals(true, $this->recipe->getRecipeSteps()->isEmpty());

        $response = $this->recipe->addRecipeStep($step_1);
        $this->recipe->addRecipeStep($setp_2);

        $this->assertInstanceOf(Recipe::class, $response);
        $this->assertEquals(true, $this->recipe->getRecipeSteps()->contains($step_1));
        $this->assertEquals(true, $this->recipe->getRecipeSteps()->contains($setp_2));
        $this->assertEquals(2, $this->recipe->getRecipeSteps()->count());

        $response = $this->recipe->removeRecipeStep($setp_2);

        $this->assertInstanceOf(Recipe::class, $response);
        $this->assertEquals(1, $this->recipe->getRecipeSteps()->count());
    }

    public function testGetRecipeIngredients(): void
    {
        $ingredient_1 = new RecipeIngredient();
        $ingredient_2 = new RecipeIngredient();

        $this->assertEquals(true, $this->recipe->getRecipeIngredients()->isEmpty());

        $response = $this->recipe->addRecipeIngredient($ingredient_1);
        $this->recipe->addRecipeIngredient($ingredient_2);

        $this->assertInstanceOf(Recipe::class, $response);
        $this->assertEquals(true, $this->recipe->getRecipeIngredients()->contains($ingredient_1));
        $this->assertEquals(true, $this->recipe->getRecipeIngredients()->contains($ingredient_2));
        $this->assertEquals(2, $this->recipe->getRecipeIngredients()->count());

        $response = $this->recipe->removeRecipeIngredient($ingredient_2);

        $this->assertInstanceOf(Recipe::class, $response);
        $this->assertEquals(1, $this->recipe->getRecipeIngredients()->count());
    }

    public function testGetRecipeTags(): void
    {
        $tag_1 = new RecipeTag();
        $tag_2 = new RecipeTag();

        $this->assertEquals(true, $this->recipe->getRecipeTags()->isEmpty());

        $response = $this->recipe->addRecipeTag($tag_1);
        $this->recipe->addRecipeTag($tag_2);

        $this->assertInstanceOf(Recipe::class, $response);
        $this->assertEquals(true, $this->recipe->getRecipeTags()->contains($tag_1));
        $this->assertEquals(true, $this->recipe->getRecipeTags()->contains($tag_2));
        $this->assertEquals(2, $this->recipe->getRecipeTags()->count());

        $response = $this->recipe->removeRecipeTag($tag_2);

        $this->assertInstanceOf(Recipe::class, $response);
        $this->assertEquals(1, $this->recipe->getRecipeTags()->count());
    }
}

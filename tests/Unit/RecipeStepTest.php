<?php

namespace App\Tests\Unit;

use App\Entity\Recipe;
use App\Entity\RecipeStep;
use PHPUnit\Framework\TestCase;

class RecipeStepTest extends TestCase
{
    private RecipeStep $step;

    protected function setUp(): void
    {
        parent::setUp();
        $this->step = new RecipeStep();
    }

    public function testGetStepNumber(): void
    {
        $value    = 1;
        $response = $this->step->setStepNumber($value);

        $this->assertInstanceOf(RecipeStep::class, $response);
        $this->assertEquals($value, $this->step->getStepNumber());
    }

    public function testGetDescription(): void
    {
        $value    = 'a step description';
        $response = $this->step->setDescription($value);

        $this->assertInstanceOf(RecipeStep::class, $response);
        $this->assertEquals($value, $this->step->getDescription());
    }

    public function testGetRecipe(): void
    {
        $recipe   = new Recipe();
        $response = $this->step->setRecipe($recipe);

        $this->assertInstanceOf(RecipeStep::class, $response);
        $this->assertEquals($recipe, $this->step->getRecipe());
    }
}

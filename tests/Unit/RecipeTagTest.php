<?php

namespace App\Tests\Unit;

use App\Entity\Recipe;
use App\Entity\RecipeTag;
use PHPUnit\Framework\TestCase;

class RecipeTagTest extends TestCase
{
    private RecipeTag $tag;

    protected function setUp(): void
    {
        parent::setUp();
        $this->tag = new RecipeTag();
    }

    public function testGetTag(): void
    {
        $value    = "a tag";
        $response = $this->tag->setTag($value);

        $this->assertInstanceOf(RecipeTag::class, $response);
        $this->assertEquals($value, $this->tag->getTag());
    }

    public function testGetRecipes(): void
    {
        $recipe_1 = new Recipe();
        $recipe_2 = new Recipe();

        $this->assertEquals(true, $this->tag->getRecipes()->isEmpty());

        $response = $this->tag->addRecipe($recipe_1);
        $this->tag->addRecipe($recipe_2);

        $this->assertInstanceOf(RecipeTag::class, $response);
        $this->assertEquals(true, $this->tag->getRecipes()->contains($recipe_1));
        $this->assertEquals(true, $this->tag->getRecipes()->contains($recipe_2));
        $this->assertEquals(2, $this->tag->getRecipes()->count());

        $response = $this->tag->removeRecipe($recipe_1);

        $this->assertInstanceOf(RecipeTag::class, $response);
        $this->assertEquals(1, $this->tag->getRecipes()->count());
    }
}

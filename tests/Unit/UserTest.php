<?php

namespace App\Tests\Unit;

use App\Entity\Meal;
use App\Entity\User;
use App\Entity\Recipe;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    private User $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = new User();
    }

    public function testGetEmail(): void
    {
        $value    = 'an.email@example.com';
        $response = $this->user->setEmail($value);

        $this->assertInstanceOf(User::class, $response);
        $this->assertEquals($value, $this->user->getEmail());
    }

    public function testGetRoles(): void
    {
        $init    = ['ROLE_USER'];

        $this->assertEquals($init, $this->user->getRoles());
        $this->assertEquals(1, \count($this->user->getRoles()));

        $value    = ['ANOTHER_ROLE'];
        $response = $this->user->setRoles($value);

        $this->assertInstanceOf(User::class, $response);
        $this->assertEquals(2, count($this->user->getRoles()));
        $this->assertTrue(in_array($init[0], $this->user->getRoles()));
        $this->assertTrue(in_array($value[0], $this->user->getRoles()));
    }

    public function testGetPassword(): void
    {
        $value    = 'a password';
        $response = $this->user->setPassword($value);

        $this->assertInstanceOf(User::class, $response);
        $this->assertEquals($value, $this->user->getPassword());
    }

    public function testGetCreatedAt(): void
    {
        $value    = new DateTimeImmutable('now');
        $response = $this->user->setCreatedAt($value);

        $this->assertInstanceOf(User::class, $response);
        $this->assertEquals($value, $this->user->getCreatedAt());
    }

    public function testRgpd(): void
    {
        $value    = false;
        $response = $this->user->setRgpd($value);

        $this->assertInstanceOf(User::class, $response);
        $this->assertEquals($value, $this->user->isRgpd());
    }

    public function testIsActive(): void
    {
        $value    = true;
        $response = $this->user->setIsActive($value);

        $this->assertInstanceOf(User::class, $response);
        $this->assertEquals($value, $this->user->isActive());
    }

    public function testGetMeals(): void
    {
        $meal_1 = new Meal();
        $meal_2 = new Meal();

        $this->assertEquals(true, $this->user->getMeals()->isEmpty());

        $response = $this->user->addMeal($meal_1);
        $this->user->addMeal($meal_2);

        $this->assertInstanceOf(User::class, $response);
        $this->assertEquals(true, $this->user->getMeals()->contains($meal_1));
        $this->assertEquals(true, $this->user->getMeals()->contains($meal_2));
        $this->assertEquals(2, $this->user->getMeals()->count());

        $response = $this->user->removeMeal($meal_2);

        $this->assertInstanceOf(User::class, $response);
        $this->assertEquals(1, $this->user->getMeals()->count());
    }

    public function testGetRecipes(): void
    {
        $recipe_1 = new Recipe();
        $recipe_2 = new Recipe();

        $this->assertEquals(true, $this->user->getRecipes()->isEmpty());

        $response = $this->user->addRecipe($recipe_1);
        $this->user->addRecipe($recipe_2);

        $this->assertInstanceOf(user::class, $response);
        $this->assertEquals(true, $this->user->getRecipes()->contains($recipe_1));
        $this->assertEquals(true, $this->user->getRecipes()->contains($recipe_2));
        $this->assertEquals(2, $this->user->getRecipes()->count());

        $response = $this->user->removeRecipe($recipe_2);

        $this->assertInstanceOf(user::class, $response);
        $this->assertEquals(1, $this->user->getRecipes()->count());
    }
}

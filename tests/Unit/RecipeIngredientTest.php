<?php

namespace App\Tests\Unit;

use App\Entity\Food;
use App\Entity\Recipe;
use PHPUnit\Framework\TestCase;
use App\Entity\RecipeIngredient;

class RecipeIngredientTest extends TestCase
{
    private RecipeIngredient $ingredient;

    protected function setUp(): void
    {
        parent::setUp();
        $this->ingredient = new RecipeIngredient();
    }

    public function testGetQuantity(): void
    {
        $value    = 50;
        $response = $this->ingredient->setQuantity($value);

        $this->assertInstanceOf(RecipeIngredient::class, $response);
        $this->assertEquals($value, $this->ingredient->getQuantity());
    }

    public function testGetRecipe(): void
    {
        $recipe    = new Recipe();
        $response = $this->ingredient->setRecipe($recipe);

        $this->assertInstanceOf(RecipeIngredient::class, $response);
        $this->assertEquals($recipe, $this->ingredient->getRecipe());
    }

    public function testGetFood(): void
    {
        $food    = new Food();
        $response = $this->ingredient->setFood($food);

        $this->assertInstanceOf(RecipeIngredient::class, $response);
        $this->assertEquals($food, $this->ingredient->getFood());
    }
}

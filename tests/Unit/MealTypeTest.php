<?php

namespace App\Tests\Unit;

use App\Entity\Meal;
use App\Entity\MealType;
use PHPUnit\Framework\TestCase;

class MealTypeTest extends TestCase
{
    private MealType $type;

    protected function setUp(): void
    {
        parent::setUp();
        $this->type = new MealType();
    }

    public function testGetType(): void
    {
        $value    = "a type";
        $response = $this->type->setType($value);

        $this->assertInstanceOf(MealType::class, $response);
        $this->assertEquals($value, $this->type->getType());
    }

    public function testGetMeals(): void
    {
        $type_1 = new Meal();
        $type_2 = new Meal();

        $this->assertEquals(true, $this->type->getMeals()->isEmpty());

        $response = $this->type->addMeal($type_1);
        $this->type->addMeal($type_2);

        $this->assertInstanceOf(MealType::class, $response);
        $this->assertEquals(true, $this->type->getMeals()->contains($type_1));
        $this->assertEquals(true, $this->type->getMeals()->contains($type_2));
        $this->assertEquals(2, $this->type->getMeals()->count());

        $response = $this->type->removeMeal($type_1);

        $this->assertInstanceOf(MealType::class, $response);
        $this->assertEquals(1, $this->type->getMeals()->count());
    }
}

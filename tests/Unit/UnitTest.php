<?php

namespace App\Tests\Unit;

use App\Entity\Food;
use App\Entity\Unit;
use PHPUnit\Framework\TestCase;

class UnitTest extends TestCase
{
    private Unit $unit;

    protected function setUp(): void
    {
        parent::setUp();
        $this->unit = new Unit();
    }

    public function testGetUnit(): void
    {
        $value    = 'gram';
        $response = $this->unit->setUnit($value);

        $this->assertInstanceOf(Unit::class, $response);
        $this->assertEquals($value, $this->unit->getUnit());
    }

    public function testGetFoods(): void
    {
        $food_1 = new Food();
        $food_2 = new Food();

        $this->assertEquals(true, $this->unit->getFoods()->isEmpty());

        $response = $this->unit->addFood($food_1);
        $this->unit->addFood($food_2);

        $this->assertInstanceOf(Unit::class, $response);
        $this->assertEquals(true, $this->unit->getFoods()->contains($food_1));
        $this->assertEquals(true, $this->unit->getFoods()->contains($food_2));
        $this->assertEquals(2, $this->unit->getFoods()->count());

        $response = $this->unit->removeFood($food_1);

        $this->assertInstanceOf(Unit::class, $response);
        $this->assertEquals(1, $this->unit->getFoods()->count());
    }
}

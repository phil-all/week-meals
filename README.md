# Week meals

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/6e87a7678dd844af983fa25b587c6b6e)](https://www.codacy.com/gl/phil-all/week-meals/dashboard?utm_source=gitlab.com&utm_medium=referral&utm_content=phil-all/week-meals&utm_campaign=Badge_Grade)

* * *

## About app

### Usage

Week meals app allows you to plan weekly menus, create recipes using pre-recorded  ingredients, while knowing the caloric intake.

### Technological choices

-   Symfony 6.1
-   PHP 8.1
-   PostgreSql 14

### Requirements

-   Docker compose

* * *

## Installation

Clone repo :

```bash
git clone https://gitlab.com/phil-all/week-meals.git <project_name>
```

Launch docker environment through [makefile](./documentation/MAKEFILE.md) command :

```bash
# project root
make start
```

Access to development bash, install composer packages and dump autoload :

```bash
# project root
make bash

#in dev bash
make vendor-up
```

Create databases (dev and test) and seed them with fixtures :

```bash
# project root
make db
```

* * *

## Additional sections

-   **[Docker environment](./documentation/DOCKER.md)**
-   **[Database](./documentation/DATA_BASE.md)**
-   **[Fixtures](./documentation/FIXTURES.md)**
-   **[Tests](./documentation/TESTS.md)**
-   **[Custom services](./documentation/CUSTOM_SERVICES.md)**
-   **[Makefile commands](./documentation/MAKEFILE.md)**

# Misc
.DEFAULT_GOAL = help
.PHONY        : help composer symfony database test


## —— Commands list 🛠️️ —————————————————————————————————————————————————————————
help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'


# Executables (local)
COMPOSER    = composer
DOCKER      = docker
DOCKER_COMP = docker-compose
SYMFONY     = bin/console
GIT			= git
PHPUNIT		= bin/phpunit
BEHAT       = ./vendor/bin/behat

# Variables
PHP = weekmeal-php


## —— Git 🔀 ———————————————————————————————————————————————————————————————————
reset: ## Reset last commit on local
	@$(GIT) reset --soft HEAD^


## —— Composer 🧙 ——————————————————————————————————————————————————————————————
update:
	@$(COMPOSER) update

dump:
	@$(COMPOSER) dump-autoload

dump-opt: ## Optimize autoloading
	@$(COMPOSER) dump-autoload --optimize

no-lock:
	rm -rf composer.lock

no-vendor:
	rm -rf vendor

install-vendor: no-vendor no-lock ## Remove and reinstall vendor
	@$(COMPOSER) install

vendor-up: install-vendor update update dump-autoload ## Remove, reinstall and update vendor


## —— Docker 🐳 ————————————————————————————————————————————————————————————————
build:
	@$(DOCKER_COMP) build --pull --no-cache

up:
	@$(DOCKER_COMP) up --detach

down: ## Stop the docker hub
	@$(DOCKER_COMP) down --remove-orphans

rm:	down ## Remove the docker hub
	$(DOCKER_COMP) rm -f

start: build up ## Build and start the containers (no logs)

restart: rm start ## Restart the docker hub

logs: ## Show live logs
	@$(DOCKER_COMP) logs --tail=0 --follow

status: ## Docker hub status
	@docker-compose ps

bash: ## Connect to the app bash
	@$(DOCKER) exec -it $(PHP) bash


## —— Symfony 🎵 ———————————————————————————————————————————————————————————————
sf: ## List all Symfony commands or pass the parameter "c=" to run a given command, example: make sf c=about
	@$(eval c ?=)
	@$(SYMFONY) $(c)

cc: c=c:c ## Clear the cache
cc: sf

user: c=make:user ## Create Symfony user
user: sf

entity: c=m:e ## Create Symfony entity
entity: sf

migration: c=make:migration ## Create Symfony migrations
migration: sf

router: c=debug:router ## Display routing
router: sf

services: c=debug:autowiring --all Service ## Display custom services
services: sf

entity: ## Create Symfony entity
	@$(SYMFONY) make:entity

controller: ## Create Symfony controller
	@$(SYMFONY) make:controller

test: ## Create Symfony test
	@$(SYMFONY) make:test

fixtures: ## Create Symfony fixtures
	@$(SYMFONY) make:fixtures


## —— Grumphp 😡 ———————————————————————————————————————————————————————————————
grum: ## Run grumphp tests
	php ./vendor/bin/grumphp run


## —— Database 🛢️ ——————————————————————————————————————————————————————————————
drop: c=d:d:d --if-exists --force ## Drop database
drop: sf

create: c=d:d:c ## Create database
create: sf

migrate: c=d:s:u -n --force ## Migrations migrate
migrate: sf

drop-test: c=d:d:d --if-exists --force --env=test ## Drop test database
drop-test: sf

create-test: c=d:d:c --env=test ## Create test database
create-test: sf

migrate-test: c=d:s:u -n --env=test --force ## Migrations migrate for test
migrate-test: sf


## —— Fixtures 🤡 ——————————————————————————————————————————————————————————————
load: c=doctrine:fixtures:load -n
load: sf

set-db:
	make drop && make create && make migrate && make load

load-test: c=doctrine:fixtures:load -n  --env=test
load-test: sf

set-db-test:
	make drop-test && make create-test && make migrate-test && make load-test

db:  ## Reset databases (dev and test) and load fixtures
	make set-db && make set-db-test


## —— Tests ✅ —————————————————————————————————————————————————————————————————
unit: ## Run unit tests only, with phpunit
	@$(PHPUNIT) tests/ --testdox

bh: ## List all Behat commands or pass the parameter "c=" to run a given command
	@$(eval c ?=)
	@$(BEHAT) $(c)

behat: ## Run functional tests only, with behat
	@$(BEHAT)

behat-list: c=-dl --lang en ## Print all available Behat expressions
behat-list: bh
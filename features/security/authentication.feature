# language: en
Feature: Authentication
    In order to access the web site
    I need to be abble to login

    Scenario: Login properly
        When I am on "/login"
        And I fill in "username" with "user1@example.com"
        And I fill in "password" with "111"
        And I press "Login"
        Then I should be on "/"
        And I should see "Logout"

    Scenario: Login incorrectly
        When I am on "/login"
        And I fill in "username" with "user1@example.com"
        And I fill in "password" with "invalid"
        And I press "Login"
        Then I should see error message "Invalid credentials"
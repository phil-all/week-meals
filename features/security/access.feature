# language: en
Feature: Application access control

    # Home page access

    Scenario: Anonymous user go to home page
        Given I am anonymous user
        When I go to "/"
        Then I am redirected to "/login"

    Scenario: Authenticated user go to home page
        Given I am logged as user "user1@example.com" with password "111"
        When I go to "/"
        Then I should be on "/"

# Recipes management page access

# Week meals management page access

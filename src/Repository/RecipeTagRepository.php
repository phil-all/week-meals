<?php

namespace App\Repository;

use App\Entity\RecipeTag;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @extends ServiceEntityRepository<RecipeTag>
 *
 * @method RecipeTag|null find($id, $lockMode = null, $lockVersion = null)
 * @method RecipeTag|null findOneBy(array $criteria, array $orderBy = null)
 * @method RecipeTag[]    findAll()
 * @method RecipeTag[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecipeTagRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RecipeTag::class);
    }

    /**
     * @SuppressWarnings(PHPMD.BooleanArgumentFlag)
     */
    public function save(RecipeTag $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @SuppressWarnings(PHPMD.BooleanArgumentFlag)
     */
    public function remove(RecipeTag $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}

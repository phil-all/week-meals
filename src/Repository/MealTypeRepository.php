<?php

namespace App\Repository;

use App\Entity\MealType;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @extends ServiceEntityRepository<MealType>
 *
 * @method MealType|null find($id, $lockMode = null, $lockVersion = null)
 * @method MealType|null findOneBy(array $criteria, array $orderBy = null)
 * @method MealType[]    findAll()
 * @method MealType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MealTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MealType::class);
    }

    /**
     * @SuppressWarnings(PHPMD.BooleanArgumentFlag)
     */
    public function save(MealType $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @SuppressWarnings(PHPMD.BooleanArgumentFlag)
     */
    public function remove(MealType $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}

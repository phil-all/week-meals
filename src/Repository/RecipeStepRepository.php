<?php

namespace App\Repository;

use App\Entity\RecipeStep;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @extends ServiceEntityRepository<RecipeStep>
 *
 * @method RecipeStep|null find($id, $lockMode = null, $lockVersion = null)
 * @method RecipeStep|null findOneBy(array $criteria, array $orderBy = null)
 * @method RecipeStep[]    findAll()
 * @method RecipeStep[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecipeStepRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RecipeStep::class);
    }

    /**
     * @SuppressWarnings(PHPMD.BooleanArgumentFlag)
     */
    public function save(RecipeStep $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @SuppressWarnings(PHPMD.BooleanArgumentFlag)
     */
    public function remove(RecipeStep $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}

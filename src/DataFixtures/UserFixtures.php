<?php

namespace App\DataFixtures;

use App\Entity\User;
use DateTimeImmutable;
use App\Service\HashPasswordProvider;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class UserFixtures extends Fixture
{
    protected HashPasswordProvider $HashPasswordProvider;

    public function __construct(HashPasswordProvider $HashPasswordProvider)
    {
        $this->HashPasswordProvider = $HashPasswordProvider;
    }


    public function load(ObjectManager $manager): void
    {
        $usersList = [
            0 => [
                'role' => 'ROLE_ADMIN',
                'email' => 'admin@example.com',
                'password' => '000',
                'active' => true
            ],
            1 => [
                'role' => 'ROLE_USER',
                'email' => 'user1@example.com',
                'password' => '111',
                'active' => true
            ],
            2 => [
                'role' => 'ROLE_USER',
                'email' => 'user2@example.com',
                'password' => '222',
                'active' => true
            ],
            3 => [
                'role' => 'ROLE_USER',
                'email' => 'user3@example.com',
                'password' => '333',
                'active' => false
            ],
        ];

        for ($i = 0; $i < count($usersList); $i++) {
            $user = new User();

            $user
                ->setEmail($usersList[$i]['email'])
                ->setPassword(
                    $this->HashPasswordProvider->getHashedPassword(
                        $user,
                        $usersList[$i]['password']
                    )
                )
                ->setCreatedAt(new DateTimeImmutable('now'))
                ->setRgpd(true)
                ->setIsActive($usersList[$i]['active'])
                ->setRoles([$usersList[$i]['role']]);

            $this->addReference('user' . $i, $user); // used in Recipe and Meal Fixtures

            $manager->persist($user);

            // Terminal summary
            // do not remove
            dump(
                \Symfony\Component\String\u('user' . $i)->padBoth(30, '-')->__toString(),
                'Role: ' . (count($user->getRoles()) == 2 ? 'Admin' : 'User'),
                ($i != 3 ? 'active' : 'NOT active')
            );
        }

        $manager->flush();
    }
}

<?php

namespace App\DataFixtures;

use App\Entity\Food;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class FoodFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $foodsList = [
            0 => [
                'name'     => 'Courgette',
                'calories' => 19,
                'unit'     => 'Gram'
            ],
            1 => [
                'name'     => 'Aubergine',
                'calories' => 35,
                'unit'     => 'Gram'
            ],
            2 => [
                'name'     => 'Poivron',
                'calories' => 29,
                'unit'     => 'Gram'
            ],
            3 => [
                'name'     => 'Oignon',
                'calories' => 30,
                'unit'     => 'Gram'
            ],
            4 => [
                'name'     => 'Tomate',
                'calories' => 16,
                'unit'     => 'Gram'
            ],
            5 => [
                'name'     => 'Concentré de tomate',
                'calories' => 92,
                'unit'     => 'Gram'
            ],
            6 => [
                'name'     => 'Bouquet garni',
                'calories' => null,
                'unit'     => 'Piece'
            ],
            7 => [
                'name'     => 'Huile d\'olive',
                'calories' => 864,
                'unit'     => 'Milliliter'
            ],
            8 => [
                'name'     => 'Sel',
                'calories' => null,
                'unit'     => 'Pinch'
            ],
            9 => [
                'name'     => 'Oeuf',
                'calories' => 145,
                'unit'     => 'Piece'
            ],
            10 => [
                'name'     => 'Penne Rigate',
                'calories' => 359,
                'unit'     => 'Gram'
            ],
            11 => [
                'name'     => 'Pomme de terre',
                'calories' => 73,
                'unit'     => 'Gram'
            ],
            12 => [
                'name'     => 'Hareng fumé',
                'calories' => 142,
                'unit'     => 'Gram'
            ],
            13 => [
                'name'     => 'Boeuf haché',
                'calories' => 332,
                'unit'     => 'Gram'
            ],
            14 => [
                'name'     => 'Courge',
                'calories' => 26,
                'unit'     => 'Gram'
            ],
            15 => [
                'name'     => 'Crème fraiche épaisse',
                'calories' => 301,
                'unit'     => 'Milliliter'
            ]
        ];

        for ($i = 0; $i < count($foodsList); $i++) {
            $food = new Food();

            $food
                ->setName($foodsList[$i]['name'])
                ->setCaloriesPerHundredUnit($foodsList[$i]['calories'])
                ->setUnit($this->getReference($foodsList[$i]['unit']));

            $this->addReference($foodsList[$i]['name'], $food); // used in RecipeIngredient Fixtures

            $manager->persist($food);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UnitFixtures::class,
        ];
    }
}

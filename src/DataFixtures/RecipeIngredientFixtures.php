<?php

namespace App\DataFixtures;

use App\Entity\Recipe;
use App\Entity\RecipeIngredient;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class RecipeIngredientFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $recipes = [
            'Ratatouille' => [
                0 => [
                    'food'     => 'Courgette',
                    'quantity' => 500
                ],
                1 => [
                    'food'     => 'Aubergine',
                    'quantity' => 500
                ],
                2 => [
                    'food'     => 'Poivron',
                    'quantity' => 250
                ],
                3 => [
                    'food'     => 'Oignon',
                    'quantity' => 80
                ],
                4 => [
                    'food'     => 'Tomate',
                    'quantity' => 300
                ],
                5 => [
                    'food'     => 'Concentré de tomate',
                    'quantity' => 25
                ],
                6 => [
                    'food'     => 'Bouquet garni',
                    'quantity' => 1
                ],
                7 => [
                    'food'     => 'Huile d\'olive',
                    'quantity' => 10
                ],
                8 => [
                    'food'     => 'Sel',
                    'quantity' => 2
                ]
            ],
            'Oeuf dur' => [
                0 => [
                    'food'     => 'Oeuf',
                    'quantity' => 1
                ]
            ],
            'Omelette au poivron' => [
                0 => [
                    'food'     => 'Poivron',
                    'quantity' => 150
                ],
                1 => [
                    'food'     => 'Oeuf',
                    'quantity' => 6
                ],
                2 => [
                    'food'     => 'Sel',
                    'quantity' => 1
                ]
            ],
            'Penne alla melanzana' => [
                0 => [
                    'food'     => 'Aubergine',
                    'quantity' => 200
                ],
                1 => [
                    'food'     => 'Penne Rigate',
                    'quantity' => 250
                ],
                2 => [
                    'food'     => 'Sel',
                    'quantity' => 2
                ],
                3 => [
                    'food'     => 'Huile d\'olive',
                    'quantity' => 5
                ],
            ],
            'Salade de harengs fumés' => [
                0 => [
                    'food'     => 'Pomme de terre',
                    'quantity' => 400
                ],
                1 => [
                    'food'     => 'Oeuf',
                    'quantity' => 4
                ],
                2 => [
                    'food'     => 'Sel',
                    'quantity' => 2
                ],
                3 => [
                    'food'     => 'Hareng fumé',
                    'quantity' => 250
                ],
            ],
            'Hachi parmentier' => [
                0 => [
                    'food'     => 'Pomme de terre',
                    'quantity' => 800
                ],
                1 => [
                    'food'     => 'Boeuf haché',
                    'quantity' => 500
                ],
                2 => [
                    'food'     => 'Sel',
                    'quantity' => 2
                ],
                3 => [
                    'food'     => 'Oignon',
                    'quantity' => 80
                ],
            ],
            'Soupe de courge' => [
                0 => [
                    'food'     => 'Pomme de terre',
                    'quantity' => 250
                ],
                1 => [
                    'food'     => 'Courge',
                    'quantity' => 500
                ],
                2 => [
                    'food'     => 'Sel',
                    'quantity' => 2
                ],
                3 => [
                    'food'     => 'Crème fraiche épaisse',
                    'quantity' => 250
                ],
            ]
        ];

        foreach ($recipes as $key => $value) {
            /** @var Recipe $recipe */
            $recipe = $this->getReference($key);

            $totalCalories = null;

            for ($i = 0; $i < count($value); $i++) {
                $ingredient = new RecipeIngredient();

                $ingredient
                    ->setRecipe($recipe)
                    ->setFood($this->getReference($value[$i]['food']))
                    ->setQuantity($value[$i]['quantity']);

                // calories calculation
                $caloriesPerHundredUnit = $ingredient->getFood()->getCaloriesPerHundredUnit();
                $ingredientCalories     = $caloriesPerHundredUnit * $value[$i]['quantity'] / 100;
                $totalCalories         += $ingredientCalories;

                $manager->persist($ingredient);
            }

            $recipe->setCaloriesPerServing((int)ceil($totalCalories / $recipe->getNumberOfSevings()));
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            RecipeFixtures::class,
        ];
    }
}

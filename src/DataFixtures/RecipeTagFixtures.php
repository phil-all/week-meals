<?php

namespace App\DataFixtures;

use App\Entity\RecipeTag;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class RecipeTagFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $recipeTagsList = [
            0 => 'Viande',
            1 => 'Poisson',
            2 => 'Soupe',
            3 => 'Entrée',
            4 => 'Plat',
            5 => 'Dessert',
            6 => 'Cuisine Provençale',
            7 => 'Cuisine Italienne'
        ];

        for ($i = 0; $i < count($recipeTagsList); $i++) {
            $tag = new RecipeTag();

            $tag->setTag($recipeTagsList[$i]);

            $this->addReference($recipeTagsList[$i], $tag); // used in Recipe Fixtures

            $manager->persist($tag);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            MealTypeFixtures::class,
        ];
    }
}

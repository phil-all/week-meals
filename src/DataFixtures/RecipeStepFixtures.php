<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Recipe;
use App\Entity\RecipeStep;
use App\Entity\RecipeIngredient;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class RecipeStepFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        for ($i = 0; $i < 7; $i++) {
            /** @var Recipe $recipe */
            $recipe = $this->getReference('Recipe' . $i);

            // Terminal summary
            // do not remove
            dump(\Symfony\Component\String\u($recipe->getName())->padBoth(30, '-')->__toString());
            /** @var RecipeIngredient $ingredient */
            foreach ($recipe->getRecipeIngredients() as $ingredient) {
                dump(
                    $ingredient->getFood()->getName() .
                        ' ' . $ingredient->getQuantity() .
                        ' ' . $ingredient->getFood()->getUnit()->getUnit()
                );
            }

            for ($j = 0; $j < rand(2, 7); $j++) {
                $step = new RecipeStep();

                $step
                    ->setStepNumber($j + 1)
                    ->setDescription($faker->paragraph())
                    ->setRecipe($recipe);

                $manager->persist($step);

                // Terminal summary
                // do not remove
                dump($step->getStepNumber() . '. ' . $step->getDescription());
            }
            dump('');
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            RecipeIngredientFixtures::class,
        ];
    }
}

<?php

namespace App\DataFixtures;

use App\Entity\MealType;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class MealTypeFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $mealTypesList = [
            0 => 'Lunch',
            1 => 'Dinner'
        ];

        for ($i = 0; $i < count($mealTypesList); $i++) {
            $type = new MealType();

            $type->setType($mealTypesList[$i]);

            $this->addReference('mealType' . $i, $type); // used in Meal Fixtures

            $manager->persist($type);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}

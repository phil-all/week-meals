<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Recipe;
use App\Entity\RecipeTag;
use App\DataFixtures\FoodFixtures;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class RecipeFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $recipesList = [
            0 => [
                'name'     => 'Ratatouille',
                'servings' => 4,
                'time'     => 45
            ],
            1 => [
                'name'     => 'Oeuf dur',
                'servings' => 1,
                'time'     => 10
            ],
            2 => [
                'name'     => 'Omelette au poivron',
                'servings' => 2,
                'time'     => 12
            ],
            3 => [
                'name'     => 'Penne alla melanzana',
                'servings' => 2,
                'time'     => 12
            ],
            4 => [
                'name'     => 'Salade de harengs fumés',
                'servings' => 4,
                'time'     => 25
            ],
            5 => [
                'name'     => 'Hachi parmentier',
                'servings' => 6,
                'time'     => 80
            ],
            6 => [
                'name'     => 'Soupe de courge',
                'servings' => 6,
                'time'     => 60
            ]
        ];

        for ($i = 0; $i < count($recipesList); $i++) {
            /** @var User  $user*/
            $user = $this->getReference('user1');

            /** @var RecipeTag $tag */
            $tag = $this->getReference('Plat');

            $recipe = new Recipe();

            $recipe
                ->setOwner($user)
                ->setName($recipesList[$i]['name'])
                ->setNumberOfSevings($recipesList[$i]['servings'])
                ->setCookingTime($recipesList[$i]['time'])
                ->addRecipeTag($tag);

            $this->addReference($recipesList[$i]['name'], $recipe); // used in RecipeStep and RecipeIngredient Fixtures
            $this->addReference('Recipe' . $i, $recipe); // used in Meal Fixtures

            $manager->persist($recipe);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            FoodFixtures::class,
        ];
    }
}

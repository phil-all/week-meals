<?php

namespace App\DataFixtures;

use DateInterval;
use App\Entity\Meal;
use App\Entity\User;
use App\Entity\Recipe;
use DateTimeImmutable;
use App\Entity\MealType;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class MealFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $nextMondayDate = (new DateTimeImmutable('now'))
            ->setTimestamp(\strtotime('next Monday'));

        for ($i = 0; $i < 14; $i++) {
            $interval = 'P' . $i . 'D';

            /** @var DateTimeImmutable $date */
            $date = $nextMondayDate->add(new DateInterval($interval));

            // Terminal summary
            // do not remove
            dump(\Symfony\Component\String\u($date->format('l'))->padBoth(30, '-')->__toString());

            for ($j = 0; $j < 2; $j++) {
                /** @var Recipe $recipe **/
                $recipe = $this->getReference('Recipe' . rand(0, 6));

                /** @var MealType $type */
                $type = $this->getReference('mealType' . $j);

                /** @var User  $user*/
                $user = $this->getReference('user1');

                $meal = new Meal();

                $meal
                    ->setDate($date)
                    ->setMealType($type)
                    ->setOwner($user)
                    ->setRecipe($recipe);

                $manager->persist($meal);

                // Terminal summary
                // do not remove
                dump(
                    $meal->getMealType()->getType() . ': ' .
                        $meal->getRecipe()->getName() . ' (' .
                        ($meal->getMealType()->getType()) . ')'
                );
            }
            dump('');
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            RecipeStepFixtures::class,
        ];
    }
}

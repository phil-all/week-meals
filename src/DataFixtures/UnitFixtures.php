<?php

namespace App\DataFixtures;

use App\Entity\Unit;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class UnitFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $unitsList = [
            0 => 'Gram',
            1 => 'Milliliter',
            2 => 'TableSpoon',
            3 => 'TeaSpoon',
            4 => 'Pinch',
            5 => 'Piece'
        ];

        for ($i = 0; $i < count($unitsList); $i++) {
            $tag = new Unit();

            $tag->setUnit($unitsList[$i]);

            $this->addReference($unitsList[$i], $tag); // used in Food Fixtures

            $manager->persist($tag);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            RecipeTagFixtures::class,
        ];
    }
}

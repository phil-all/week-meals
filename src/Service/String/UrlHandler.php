<?php

namespace App\Service\String;

/**
 * UrlHandler provides methods to handles strings related to URL.
 *
 * @author Philippe Allard-Latour <phil.all.dev@gmail.com>
 */
class UrlHandler
{
    private const DOMAIN_NAME = '127.0.0.1:8110';

    /**
     * Replace 'localhost' by domain name in URL.
     */
    public function formatLocalUrl(string $url): string
    {
        return preg_replace('/localhost/', self::DOMAIN_NAME, $url);
    }

    /**
     * Get the absolute URL from a given path.
     */
    public function pathToAbsoluteUrl(string $path): string
    {
        return 'http://' . self::DOMAIN_NAME . $path;
    }
}

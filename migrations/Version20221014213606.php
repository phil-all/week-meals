<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221014213606 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'create database';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE food_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE meal_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE meal_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE recipe_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE recipe_ingredient_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE recipe_step_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE recipe_tag_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE unit_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "user_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql(
            'CREATE TABLE food (
                id INT NOT NULL,
                unit_id INT NOT NULL,
                name VARCHAR(120) NOT NULL,
                calories_per_hundred_unit SMALLINT DEFAULT NULL,
                PRIMARY KEY(id)
            )'
        );
        $this->addSql('CREATE INDEX IDX_D43829F7F8BD700D ON food (unit_id)');
        $this->addSql(
            'CREATE TABLE meal (
                id INT NOT NULL,
                owner_id INT NOT NULL,
                recipe_id INT NOT NULL,
                meal_type_id INT NOT NULL,
                planned_date DATE NOT NULL,
                PRIMARY KEY(id)
            )'
        );
        $this->addSql('CREATE INDEX IDX_9EF68E9C7E3C61F9 ON meal (owner_id)');
        $this->addSql('CREATE INDEX IDX_9EF68E9C59D8A214 ON meal (recipe_id)');
        $this->addSql('CREATE INDEX IDX_9EF68E9CBCFF3E8A ON meal (meal_type_id)');
        $this->addSql('COMMENT ON COLUMN meal.planned_date IS \'(DC2Type:date_immutable)\'');
        $this->addSql(
            'CREATE TABLE meal_type (
                id INT NOT NULL,
                type VARCHAR(10) NOT NULL,
                PRIMARY KEY(id)
            )'
        );
        $this->addSql(
            'CREATE TABLE recipe (
                id INT NOT NULL,
                owner_id INT NOT NULL,
                name VARCHAR(120) NOT NULL,
                number_of_sevings SMALLINT NOT NULL,
                calories_per_serving SMALLINT DEFAULT NULL,
                cooking_time SMALLINT DEFAULT NULL,
                PRIMARY KEY(id)
            )'
        );
        $this->addSql('CREATE INDEX IDX_DA88B1377E3C61F9 ON recipe (owner_id)');
        $this->addSql('CREATE TABLE recipe_recipe_tag (recipe_id INT NOT NULL, recipe_tag_id INT NOT NULL, PRIMARY KEY(recipe_id, recipe_tag_id))');
        $this->addSql('CREATE INDEX IDX_3BA055AC59D8A214 ON recipe_recipe_tag (recipe_id)');
        $this->addSql('CREATE INDEX IDX_3BA055AC37CC7D30 ON recipe_recipe_tag (recipe_tag_id)');
        $this->addSql(
            'CREATE TABLE recipe_ingredient (
                id INT NOT NULL,
                recipe_id INT NOT NULL,
                food_id INT NOT NULL,
                quantity SMALLINT NOT NULL,
                PRIMARY KEY(id)
            )'
        );
        $this->addSql('CREATE INDEX IDX_22D1FE1359D8A214 ON recipe_ingredient (recipe_id)');
        $this->addSql('CREATE INDEX IDX_22D1FE13BA8E87C4 ON recipe_ingredient (food_id)');
        $this->addSql(
            'CREATE TABLE recipe_step (
                id INT NOT NULL,
                recipe_id INT NOT NULL,
                step_number SMALLINT NOT NULL,
                description TEXT NOT NULL,
                PRIMARY KEY(id)
            )'
        );
        $this->addSql('CREATE INDEX IDX_3CA2A4E359D8A214 ON recipe_step (recipe_id)');
        $this->addSql(
            'CREATE TABLE recipe_tag (
                id INT NOT NULL,
                tag VARCHAR(80) NOT NULL,
                PRIMARY KEY(id)
            )'
        );
        $this->addSql(
            'CREATE TABLE unit (
                id INT NOT NULL,
                unit VARCHAR(12) NOT NULL,
                PRIMARY KEY(id)
            )'
        );
        $this->addSql(
            'CREATE TABLE "user" (
                id INT NOT NULL,
                email VARCHAR(180) NOT NULL,
                roles JSON NOT NULL,
                password VARCHAR(255) NOT NULL,
                created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
                rgpd BOOLEAN NOT NULL,
                is_active BOOLEAN NOT NULL,
                PRIMARY KEY(id)
            )'
        );
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('COMMENT ON COLUMN "user".created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql(
            'CREATE TABLE messenger_messages (
                id BIGSERIAL NOT NULL,
                body TEXT NOT NULL,
                headers TEXT NOT NULL,
                queue_name VARCHAR(190) NOT NULL,
                created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
                available_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
                delivered_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
                PRIMARY KEY(id)
            )'
        );
        $this->addSql('CREATE INDEX IDX_75EA56E0FB7336F0 ON messenger_messages (queue_name)');
        $this->addSql('CREATE INDEX IDX_75EA56E0E3BD61CE ON messenger_messages (available_at)');
        $this->addSql('CREATE INDEX IDX_75EA56E016BA31DB ON messenger_messages (delivered_at)');
        $this->addSql('CREATE OR REPLACE FUNCTION notify_messenger_messages() RETURNS TRIGGER AS $$
            BEGIN
                PERFORM pg_notify(\'messenger_messages\', NEW.queue_name::text);
                RETURN NEW;
            END;
        $$ LANGUAGE plpgsql;');
        $this->addSql('DROP TRIGGER IF EXISTS notify_trigger ON messenger_messages;');
        $this->addSql('CREATE TRIGGER notify_trigger AFTER INSERT OR UPDATE ON messenger_messages FOR EACH ROW EXECUTE PROCEDURE notify_messenger_messages();');
        $this->addSql('ALTER TABLE food ADD CONSTRAINT FK_D43829F7F8BD700D FOREIGN KEY (unit_id) REFERENCES unit (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE meal ADD CONSTRAINT FK_9EF68E9C7E3C61F9 FOREIGN KEY (owner_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE meal ADD CONSTRAINT FK_9EF68E9C59D8A214 FOREIGN KEY (recipe_id) REFERENCES recipe (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE meal ADD CONSTRAINT FK_9EF68E9CBCFF3E8A FOREIGN KEY (meal_type_id) REFERENCES meal_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE recipe ADD CONSTRAINT FK_DA88B1377E3C61F9 FOREIGN KEY (owner_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE recipe_recipe_tag ADD CONSTRAINT FK_3BA055AC59D8A214 FOREIGN KEY (recipe_id) REFERENCES recipe (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE recipe_recipe_tag ADD CONSTRAINT FK_3BA055AC37CC7D30 FOREIGN KEY (recipe_tag_id) REFERENCES recipe_tag (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE recipe_ingredient ADD CONSTRAINT FK_22D1FE1359D8A214 FOREIGN KEY (recipe_id) REFERENCES recipe (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE recipe_ingredient ADD CONSTRAINT FK_22D1FE13BA8E87C4 FOREIGN KEY (food_id) REFERENCES food (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE recipe_step ADD CONSTRAINT FK_3CA2A4E359D8A214 FOREIGN KEY (recipe_id) REFERENCES recipe (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE food_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE meal_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE meal_type_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE recipe_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE recipe_ingredient_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE recipe_step_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE recipe_tag_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE unit_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE "user_id_seq" CASCADE');
        $this->addSql('ALTER TABLE food DROP CONSTRAINT FK_D43829F7F8BD700D');
        $this->addSql('ALTER TABLE meal DROP CONSTRAINT FK_9EF68E9C7E3C61F9');
        $this->addSql('ALTER TABLE meal DROP CONSTRAINT FK_9EF68E9C59D8A214');
        $this->addSql('ALTER TABLE meal DROP CONSTRAINT FK_9EF68E9CBCFF3E8A');
        $this->addSql('ALTER TABLE recipe DROP CONSTRAINT FK_DA88B1377E3C61F9');
        $this->addSql('ALTER TABLE recipe_recipe_tag DROP CONSTRAINT FK_3BA055AC59D8A214');
        $this->addSql('ALTER TABLE recipe_recipe_tag DROP CONSTRAINT FK_3BA055AC37CC7D30');
        $this->addSql('ALTER TABLE recipe_ingredient DROP CONSTRAINT FK_22D1FE1359D8A214');
        $this->addSql('ALTER TABLE recipe_ingredient DROP CONSTRAINT FK_22D1FE13BA8E87C4');
        $this->addSql('ALTER TABLE recipe_step DROP CONSTRAINT FK_3CA2A4E359D8A214');
        $this->addSql('DROP TABLE food');
        $this->addSql('DROP TABLE meal');
        $this->addSql('DROP TABLE meal_type');
        $this->addSql('DROP TABLE recipe');
        $this->addSql('DROP TABLE recipe_recipe_tag');
        $this->addSql('DROP TABLE recipe_ingredient');
        $this->addSql('DROP TABLE recipe_step');
        $this->addSql('DROP TABLE recipe_tag');
        $this->addSql('DROP TABLE unit');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('DROP TABLE messenger_messages');
    }
}

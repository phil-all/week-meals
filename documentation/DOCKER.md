# Doker

Return to [README](../README.md).

## Files and folders

```bash
project/
├─ doker/
│  ├─ db/
│  │  └─ data/  # contains database volume
│  │     └─ ...
│  ├─ nginx/
│  │  ├─ default.conf
│  │  └─ Dockerfile
│  └─ php/
│     ├─ Dockerfile
│     ├─ php.ini
│     └─ xdebug.ini
└─ docker-compose.yml
```

## Services

**database**

-   name  : weekmeal-db
-   image : postgres-14-alpine

**web**

-   name  : weekmeal-nginx
-   image : nginx (latest)

**php**

-   name  : weekmeal-php
-   image : php:8.1-fpm

## Ports

**website**  available on 127.0.0.1:8110
**database** use port 5435

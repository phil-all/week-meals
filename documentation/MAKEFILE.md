# Makefile

Return to [README](../README.md).

## Description

A makefile is available in root project and contains many commands shortcut.

```bash
 —— Commands list 🛠️️ ————————————————————————————————————————————————————————— 
help                           Outputs this help screen
 —— Composer 🧙 —————————————————————————————————————————————————————————————— 
dump-opt                       Optimize autoloading
install-vendor                 Remove and reinstall vendor
vendor-up                      Remove, reinstall and update vendor
 —— Docker 🐳 ———————————————————————————————————————————————————————————————— 
down                           Stop the docker hub
rm                             Remove the docker hub
start                          Build and start the containers (no logs)
restart                        Restart the docker hub
logs                           Show live logs
status                         Docker hub status
bash                           Connect to the app bash
 —— Symfony 🎵 ——————————————————————————————————————————————————————————————— 
sf                             List all Symfony commands or pass the parameter "c=" to run a given command, example: make sf c=about
cc                             Clear the cache
user                           Create Symfony user
entity                         Create Symfony entity
migration                      Create Symfony migrations
router                         Display routing
entity                         Create Symfony entity
test                           Create Symfony test
fixtures                       Create Symfony fixtures
 —— Grumphp 😡 ——————————————————————————————————————————————————————————————— 
grum                           Run grumphp tests
 —— Database 🛢️ —————————————————————————————————————————————————————————————— 
drop                           Drop database
create                         Create database
migrate                        Migrations migrate
drop-test                      Drop test database
create-test                    Create test database
migrate-test                   Migrations migrate for test
 —— Fixtures 🤡 —————————————————————————————————————————————————————————————— 
db                             Reset databases (dev and test) and load fixtures
 —— Tests ✅ ————————————————————————————————————————————————————————————————— 
unit                           Run unit tests only
```

## Usage

Use command as follow :

```bash
# project root
make <command>
```

# Data base

Return to [README](../README.md).

## System

To have an object-relational database system, we use PostgreSQL.

Port:

-   5432

Demo user:

-   name: user
-   password: pass

## Model

Green entities are accessible by authenticated users, and the blue ones only by admin.

![Data Model](./data-base/data-model.png)

## Entities

### user

**Description**

The user.

**Fields**

-   **id** is a `Int` - the unique user identifier.
-   **email** is a `Varchar(180)` - the user email address.
-   **password** is a `Varchar(180)` - the user password.
-   **created_at** is a `Date` - the user account creation date.
-   **rgpd** is a `Boolean` - the user General Data Protection Regulation consent.
-   **is_active** is a `Boolean` - the user account activation from link send by email to user after registration.
-   **roles** is a `JSON` - the user roles (eg: user, admin)

### recipe

**Description**

The recipe description.

**Fields**

-   **id** is an `Int` - the unique recipe identifier.
-   **name** is a `Varchar(120)` - the recipe name.
-   **number_of_servings** is a `Smallint` - the number of servings of this recipe.
-   **calories_per_serving** is a `Smallint` - the total calories for one serving.
-   **cooking_time** is a `Smallint` - the recipe cooking time in minutes.
-   **owner_id** is an `Int` - the recipe owner unique identifier.

### recipe_tag

**Description**

The tag of one or more recipe.

**Fields**

-   **id** is an `Int` - the unique tag identifier.
-   **tag** is a `Varchar(80)` - the tag.

### recipe_step

**Description**

The step of a specified recipe.

**Fields**

-   **id** is an `Int` - the unique step identifier.
-   **step_number** is a `Smallint` - the position in the recipe process.
-   **description** is a `Text` - the complete descrition of the step.

### recipe_ingredient

**Description**

The ingredient of a specified recipe.

**Fields**

-   **id** is an `Int` - the unique ingredient identifier.
-   **quantity** is a `Smallint` - the quatity of the ingredient (in a specific unit describe by the unit link to the food).
-   **food_id** is an `Int` - the unique food identifier.
-   **recipe_id** is an `Int` - the unique recipe identifier.

### food

**Description**

The food.

**Fields**

-   **id** is an `Int` - the unique food identifier.
-   **name** is a Varchar(120) - the food name.
-   **calories_per_hundred** is a `Smallint` - the number of calories for hundred units.
-   **unit_id** is an `Int` - the unique unit identifier.

### unit

**Description**

The unit (eg: piece, gram, liter).

**Fields**

-   **id** is an `Int` - the unique unit identifier.
-   **unit** is a Varchar(12) - the unit name.

### meal

**Description**

The planned meal.

**Fields**

-   **id** is an `Int` - the unique meal identifier.
-   **planned_date** is a `Date` - the meal planed date.
-   **meal_type_id** is an `Int` - the meal type unique identifier.
-   **recipe_id** is an `Int` - the recipe unique identifier.
-   **owner_id** is an `Int` - the meal owner unique identifier.

### meal_type

**Description**

The meal type (eg: breakfast, lunch, snack, dinner).

**Fields**

-   **id** is an `Int` - the unique meal type identifier.
-   **type** is a \`Varchar(10) - the type of meal.

# Custom services

Return to [README](../README.md).

Custom services are stored in `./src/Service/`.

Obtain list in terminal :

```bash
# project root
make services
```

## Strings handlers

### URLHandler

UrlHandler provides methods to handles strings related to URL.

## Encription

### HashPasswordProvider

HashPasswordProvider provides hashed password from a given user and a plain password.

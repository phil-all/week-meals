# Tests

Return to [README](../README.md).

* * *

## Unit tests

Unit tests are done with Phpunit.

Run unit tests :

```bash
make unit
```

* * *

## Functional tests

Functional tests are made with Behat.

Run functional tests :

```bash
make behat
```

Display available Behat expressions :

```bash
make behat-list
```

### Contexts

In order to complete Mink context, new context is implemented.

#### WebContext

This context class contains the definitions of the steps used by the security feature files.

It contains the following expresisons:

-   Given I am anonymous user
-   Given I am logged as user **:userIdentifier** with password **:password**
-   Then I am redirected to **:path**
-   Then I should see error message **:message**

### Features

#### Security

Located in `./features/security` folder.

-   Authentication
-   Access control
